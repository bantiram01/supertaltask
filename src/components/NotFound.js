/* 
      *                                                            *
    *****                                                        *****                             
      *                                                            *
        ==========================================================
        ==========                                      ==========
        ==========        Page for Not-Found            ==========
        ==========                                      ==========
        ==========================================================
      *                                                            *
    *****                                                        *****   
      *                                                            *
*/

import React from "react";
import { Link } from "react-router-dom";

const NotFound = () => {
  return (
    <Link to="/">
      <img
        src="https://doyouconvert.com/wp-content/uploads/2018/04/404_Error.jpg"
        width="100%"
      />
    </Link>
  );
};

export default NotFound;
