import React, { useState } from "react";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  NavbarText,
} from "reactstrap";
import { Link } from "react-router-dom";

// header for the pages which are public
const Header = (props) => {
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);

  return (
    <div>
      <Navbar color="light" light expand="md">
        <Link to="/">
          <NavbarBrand>Supertal Assignment</NavbarBrand>
        </Link>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <NavItem>
              <Link to="/">
                <NavLink>Home</NavLink>
              </Link>
            </NavItem>

            <NavItem>
              <NavLink href="https://bitbucket.org/bantiram01/supertaltask/src">
                GitHub
              </NavLink>
            </NavItem>
          </Nav>
          <NavbarText>Banti Ram</NavbarText>
        </Collapse>
      </Navbar>
    </div>
  );
};

export default Header;
