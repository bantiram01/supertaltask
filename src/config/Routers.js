import React from "react";
import { BrowserRouter, Switch } from "react-router-dom";
import AppRoute from "./AppRoute";
import NotFound from "../components/NotFound";
import Home from "../containers/landingPage";
import { publicLayout } from "../components/Layouts";

// application router goes here...
const Routers = ({ store, history }) => {
  return (
    <BrowserRouter>
      <Switch>
        <AppRoute
          exact={true}
          path="/"
          component={Home}
          layout={publicLayout}
        />

        <AppRoute path="*" component={NotFound} layout={publicLayout} />
      </Switch>
    </BrowserRouter>
  );
};
export default Routers;
