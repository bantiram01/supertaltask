import React from "react";
import { PersistGate } from "redux-persist/es/integration/react";
import { Provider, connect } from "react-redux";
import createHistory from "history/createBrowserHistory";
import Routers from "./Routers";
import configureStore from "./configureStore";
import Loader from "../components/Loader";

export const history = createHistory();

// store configration
const { persistor, store } = configureStore(history);

//global loader for the whole app
const ReduxLoader = connect((state) => ({ loading: state.app.isLoading }))(
  Loader
);

export default () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <Routers store={store} history={history} />
        <ReduxLoader />
      </PersistGate>
    </Provider>
  );
};
