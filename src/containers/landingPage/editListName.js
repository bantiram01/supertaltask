import React from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
} from "reactstrap";
const EditListName = (props) => {
  return (
    <Modal isOpen={props.editTitleModal} toggle={props.toggleEditTitleModal}>
      <ModalHeader toggle={props.toggleEditTitleModal}>Edit list</ModalHeader>
      <ModalBody>
        <p>Enter title</p>
        <InputGroup>
          <InputGroupAddon addonType="prepend">
            <InputGroupText>
              <i class="fa fa-comment"></i>
            </InputGroupText>
          </InputGroupAddon>
          <Input
            value={props.listName}
            onChange={(e) => props.changeListName(e)}
          />
        </InputGroup>
      </ModalBody>
      <ModalFooter>
        <Button color="primary" onClick={props.updateListName}>
          Update
        </Button>{" "}
        <Button color="secondary" onClick={props.toggleEditTitleModal}>
          Cancel
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default EditListName;
