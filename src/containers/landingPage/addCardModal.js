import React from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  UncontrolledButtonDropdown,
  DropdownMenu,
  DropdownToggle,
} from "reactstrap";
const AddCardModal = (props) => {
  return (
    <Modal isOpen={props.addCardModal} toggle={props.toggleAddCardModal}>
      <ModalHeader toggle={props.toggleAddCardModal}>Add a card</ModalHeader>
      <ModalBody>
        <p>Enter Description</p>
        <InputGroup>
          <InputGroupAddon addonType="prepend">
            <InputGroupText>
              <i className="fa fa-comment"></i>
            </InputGroupText>
          </InputGroupAddon>
          <Input
            type="textarea"
            name="cardDescription"
            onChange={(e) => props.handleCardDetails(e)}
          />
        </InputGroup>
        <p>User to assign</p>
        <InputGroup>
          <InputGroupAddon addonType="prepend">
            <InputGroupText>
              <i className="fa fa-comment"></i>
            </InputGroupText>
          </InputGroupAddon>
          <Input
            name="assignedUsername"
            onChange={(e) => props.handleCardDetails(e)}
          />
        </InputGroup>
        <p>Status</p>
        <InputGroup>
          <InputGroupAddon addonType="prepend">
            <InputGroupText>
              <i class="fa fa-comment"></i>
            </InputGroupText>
          </InputGroupAddon>
          <UncontrolledButtonDropdown>
            <DropdownToggle caret>{props.statusDropdown}</DropdownToggle>
            <DropdownMenu>{props.renderDropdownItems()}</DropdownMenu>
          </UncontrolledButtonDropdown>
        </InputGroup>
      </ModalBody>
      <ModalFooter>
        <Button color="primary" onClick={props.addCard}>
          Submit
        </Button>{" "}
        <Button color="secondary" onClick={props.toggleAddCardModal}>
          Cancel
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default AddCardModal;
