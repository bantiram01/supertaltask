import React from "react";
import {
  Button,
  UncontrolledButtonDropdown,
  DropdownMenu,
  DropdownToggle,
} from "reactstrap";
const Card = (props) => {
  let { lists, item } = props,
    index = lists.findIndex((data) => data.id === item.listId);
  return (
    <div className="item" key={Math.random()}>
      <p className="title">Assigned To:{item.assignedUsername}</p>
      <p className="body">Description: {item.cardDescription}</p>
      Status:{" "}
      <UncontrolledButtonDropdown>
        <DropdownToggle caret>
          {lists[index] && lists[index].name}
        </DropdownToggle>
        <DropdownMenu>
          {props.renderStatusChangeDropdownItems(lists, item)}
        </DropdownMenu>
      </UncontrolledButtonDropdown>
      <br />
      <div style={{ textAlign: "right" }}>
        <Button
          size="sm"
          color="warning"
          className="marginTop5"
          onClick={() => props.handleCardDelete(item)}
        >
          Delete <i className="fa fa-trash"></i>
        </Button>{" "}
      </div>
    </div>
  );
};

export default Card;
