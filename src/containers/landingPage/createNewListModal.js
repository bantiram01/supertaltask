import React from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
} from "reactstrap";
const NewListModal = (props) => {
  return (
    <Modal isOpen={props.showModal} toggle={props.toggle}>
      <ModalHeader toggle={props.toggle}>Add a new list</ModalHeader>
      <ModalBody>
        <p>Enter title</p>
        <InputGroup>
          <InputGroupAddon addonType="prepend">
            <InputGroupText>
              <i class="fa fa-comment"></i>
            </InputGroupText>
          </InputGroupAddon>
          <Input onChange={(e) => props.changeListName(e)} />
        </InputGroup>
      </ModalBody>
      <ModalFooter>
        <Button color="primary" onClick={props.createNewList}>
          Submit
        </Button>{" "}
        <Button color="secondary" onClick={props.toggle}>
          Cancel
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default NewListModal;
