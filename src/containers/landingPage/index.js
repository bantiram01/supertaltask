import React from "react";
import { Container, Row, Col } from "reactstrap";
import { Button, ButtonGroup, DropdownItem } from "reactstrap";
import { connect } from "react-redux";
import _ from "underscore";
import { bindActionCreators } from "redux";
import {
  createNewList,
  deleteList,
  addCardToList,
  changeCardStatus,
  reorderListCards,
  deleteCard,
  updateListName,
} from "../../redux/app/actions";
import NewListModal from "./createNewListModal";
import AddCardModal from "./addCardModal";
import Lists from "./lists";
import Card from "./card";
import EditListName from "./editListName";

class SuperTal extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      showModal: false,
      addCardModal: false,
      listName: "",
      statusDropdown: "Choose One",
      editTitleModal: false,
      cardDescription: "",
      assignedUsername: "",
      listIdToBeUpdated: "",
    };

    this.itemRenderer = this.itemRenderer.bind(this);
    this.handleRLDDChange = this.handleRLDDChange.bind(this);
    this.debouncedReorderAction = _.debounce(this.props.reorderListCards, 1000);
  }

  toggle = () =>
    this.setState({
      showModal: !this.state.showModal,
    });

  toggleAddCardModal = () =>
    this.setState({
      addCardModal: !this.state.addCardModal,
    });

  toggleEditTitleModal = ({ name, id }) => {
    this.setState({
      editTitleModal: !this.state.editTitleModal,
      listName: name,
      listIdToBeUpdated: id,
    });
  };

  updateListName = () => {
    let { listName, listIdToBeUpdated } = this.state,
      updateListNameObj = {
        listName,
        listIdToBeUpdated,
      };

    this.props.updateListName(updateListNameObj);
    this.setState({
      editTitleModal: false,
    });
  };

  createNewList = () => {
    let newListObj = {
      name: this.state.listName,
      id: _.random(3, 1000),
      cards: [],
    };
    this.props.createNewList(newListObj);
    this.toggle();
  };

  deleteList = (id) => {
    let deleteListObj = {
      id,
    };
    this.props.deleteList(deleteListObj);
  };

  addCard = () => {
    let { cardDescription, assignedUsername, statusDropdown } = this.state,
      { lists } = this.props.app,
      index = lists.findIndex((data) => data.name === statusDropdown);

    let addCardObj = {
      cardDescription,
      assignedUsername,
      listId: lists[index].id,
      id: Math.random(),
    };
    this.props.addCardToList(addCardObj);
    this.toggleAddCardModal();
  };

  setDropdownValue = (data) => {
    this.setState({
      statusDropdown: data.name,
    });
  };

  renderDropdownItems = () => {
    let { lists } = this.props.app;
    return (
      lists.length > 0 &&
      lists.map((data, index) => {
        return (
          <DropdownItem
            key={Math.random()}
            onClick={() => this.setDropdownValue(data)}
          >
            {data.name}
          </DropdownItem>
        );
      })
    );
  };

  renderStatusChangeDropdownItems = (lists, item) => {
    return (
      lists.length > 0 &&
      lists.map((data, index) => {
        return (
          <DropdownItem
            key={Math.random()}
            onClick={() => this.changeCardStatus(data, item)}
          >
            {data.name}
          </DropdownItem>
        );
      })
    );
  };

  changeCardStatus = (data, item) => {
    let changeStatusObj = {
      card: item,
      listIdFrom: item.listId,
      moveToList: data.id,
    };
    this.props.changeCardStatus(changeStatusObj);
  };

  itemRenderer(item) {
    let { lists } = this.props.app;
    return (
      <Card
        lists={lists}
        item={item}
        renderStatusChangeDropdownItems={this.renderStatusChangeDropdownItems}
        handleCardDelete={this.props.deleteCard}
      />
    );
  }

  handleRLDDChange(reorderedItems, index) {
    let reorderCardsObj = {
      reorderedItems,
      index,
    };
    this.debouncedReorderAction(reorderCardsObj);
  }

  changeListName = (e) => {
    this.setState({ listName: e.target.value });
  };

  handleCardDetails = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  render() {
    const {
        showModal,
        addCardModal,
        editTitleModal,
        statusDropdown,
        listName,
      } = this.state,
      { lists } = this.props.app;
    return (
      <div>
        <Container fluid>
          <Row className="marginTop5">
            <Col
              style={{
                textAlign: "right",
              }}
            >
              <ButtonGroup>
                <Button size="lg" color="warning" onClick={this.toggle}>
                  Create a new list <i className="fa fa-plus"></i>
                </Button>
                <Button
                  size="lg"
                  color="info"
                  onClick={() => this.toggleAddCardModal()}
                >
                  Add Card <i className="fa fa-plus"></i>
                </Button>
              </ButtonGroup>
            </Col>
          </Row>

          <Row className="marginTop5">
            <Lists
              lists={lists}
              deleteList={this.deleteList}
              itemRenderer={this.itemRenderer}
              handleRLDDChange={this.handleRLDDChange}
              toggleEditTitleModal={this.toggleEditTitleModal}
            />
          </Row>
          <NewListModal
            showModal={showModal}
            toggle={this.toggle}
            changeListName={this.changeListName}
            createNewList={this.createNewList}
          />

          <AddCardModal
            addCardModal={addCardModal}
            toggleAddCardModal={this.toggleAddCardModal}
            handleCardDetails={this.handleCardDetails}
            addCard={this.addCard}
            statusDropdown={statusDropdown}
            renderDropdownItems={this.renderDropdownItems}
          />

          <EditListName
            editTitleModal={editTitleModal}
            toggleEditTitleModal={this.toggleEditTitleModal}
            listName={listName}
            changeListName={this.changeListName}
            updateListName={this.updateListName}
          />
        </Container>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    app: state.app,
  };
};

const mapDispatchToProps = (dispatch) => ({
  createNewList: bindActionCreators(createNewList, dispatch),
  deleteList: bindActionCreators(deleteList, dispatch),
  addCardToList: bindActionCreators(addCardToList, dispatch),
  changeCardStatus: bindActionCreators(changeCardStatus, dispatch),
  reorderListCards: bindActionCreators(reorderListCards, dispatch),
  deleteCard: bindActionCreators(deleteCard, dispatch),
  updateListName: bindActionCreators(updateListName, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(SuperTal);
