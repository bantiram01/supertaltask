import React from "react";
import RLDD from "react-list-drag-and-drop/lib/RLDD";
import { Button, Row, Col, ButtonGroup } from "reactstrap";

const RenderLists = (props) => {
  return (
    props.lists.length > 0 &&
    props.lists.map((data, index) => {
      return (
        <Col key={Math.random()}>
          <Row className="listHeader">
            <Col>
              <h5>{data.name}</h5>
            </Col>
            <Col>
              <ButtonGroup>
                <Button
                  color="info"
                  onClick={() => props.toggleEditTitleModal(data)}
                >
                  Edit <i className="fa fa-pencil"></i>
                </Button>
                <Button
                  color="danger"
                  onClick={() => props.deleteList(data.id)}
                >
                  Delete <i className="fa fa-trash"></i>
                </Button>
              </ButtonGroup>
            </Col>
          </Row>
          <br />
          {data.cards && data.cards.length > 0 ? (
            <RLDD
              cssClasses="example"
              items={data.cards && data.cards.length > 0 ? data.cards : []}
              itemRenderer={props.itemRenderer}
              onChange={(reorderedList) =>
                props.handleRLDDChange(reorderedList, index)
              }
            />
          ) : (
            <p>Nothing here! Move or add cards to the list..</p>
          )}
        </Col>
      );
    })
  );
};

export default RenderLists;
