import * as TYPE from "./types";

export const hideLoader = () => ({ type: TYPE.HIDE_LOADER });
export const showLoader = () => ({ type: TYPE.SHOW_LOADER });
export const createNewList = (data) => ({
  type: TYPE.CREATE_NEW_LIST,
  data: data,
});

export const deleteList = (data) => ({
  type: TYPE.DELETE_LIST,
  data,
});

export const addCardToList = (data) => ({
  type: TYPE.ADD_CARD_TO_LIST,
  data,
});

export const changeCardStatus = (data) => ({
  type: TYPE.CHANGE_CARD_STATUS,
  data,
});

export const reorderListCards = (data) => ({
  type: TYPE.REORDER_LIST_CARDS,
  data,
});

export const deleteCard = (data) => ({
  type: TYPE.DELETE_CARD,
  data,
});

export const updateListName = (data) => ({
  type: TYPE.UPDATE_LIST_NAME,
  data,
});
