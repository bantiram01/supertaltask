import createCompressor from "redux-persist-transform-compress";

const compressor = createCompressor();

export default compressor;
