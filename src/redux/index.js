import { persistCombineReducers } from "redux-persist";
import storage from "redux-persist/lib/storage";
import { routerReducer } from "react-router-redux";
import encryptor from "./encryptor";
import compressor from "./compressor";
import app from "./app";

// store configurations
const storeConfig = {
  key: "primary",
  storage: storage,
  transforms: [encryptor, compressor],
};

// Combine all the reducers into one
const superTalTask = persistCombineReducers(storeConfig, {
  app,
  routerReducer,
});

export default superTalTask;
